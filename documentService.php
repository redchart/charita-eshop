<?php

namespace App\Models;

use App\SklipekException;
use Contributte\Monolog\LoggerManager;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use App\Sklipek;
use Sybase\SybaseException;
use Sybase\SybasePdoDriver;
use App\Entity\Document;
use Throwable;

//use SmartObject;
class DocService{

    public const DOMAIN_CZ = 1;
    public const DOMAIN_SK  = 6;

    public const ORDER_ASC  = 0;
    public const ORDER_DESC  = 1;

    public const ORDER_FILE_NAME = 1;
    public const ORDER_FILE_SIZE = 1;
    public const ORDER_DATE = 1;


    private LoggerInterface $logger;
    private SybasePdoDriver $database;
    private Sklipek $sklipek;

    public function __construct(SybasePdoDriver $database, LoggerManager $logger, Sklipek $sklipek) {
        $this->database = $database;
        $this->logger = $logger->get("database");
        $this->sklipek = $sklipek;
    }

    public function getDocumentsList(string $name = 'null', int $domain = self::DOMAIN_CZ, ?int $sort_by = self::ORDER_DATE , ?int $order = self::ORDER_DESC):array{
        $list = [];

        try {
            $rows = $this->database->query("procedura ?, ?, ?, ?", $domain, $name, $sort_by, $order)->fetchAll();
            if(!empty($rows)) {
                foreach ($rows as $row) {

                    $doc = new Document(
                        $row->a,
                        $row->b,
                        $row->c,
                        $row->d,
                        $row->a,
                        null,
                        $row->b,
                        $row->c
                    );

                    $list[] = $doc;
                }
            }

        }catch (SybaseException $e){
            $this->logger->error($e->getMessage());
            throw new FileManException("Nepodařilo se načíst dokumenty");
        }
        return $list;
    }


    public function saveDocuments($documents, int $domain, string $user_name){
        foreach ($documents as $document ){
            //$this->saveDocument($document);
            $name = $document->getSanitizedName();
            $content = $document->getContents();
            if(!$name){
                throw new InvalidArgumentException("invalid file name");
            }
            if(!$content){
                throw new InvalidArgumentException("file is empty");
            }

            $error_docs = [];
            $stored_docs = [];
            try {
                $this->saveDocument(
                    new Document(
                        $name
                        , $document->getSize()
                        , null
                        , date("d.m.Y - H:i:s")
                        , null
                        , $document->getContents()
                        , $domain
                        , $user_name
                    )

                );
                $stored_docs[] = $name;
            }catch (SklipekException|SybaseException $e){
                $this->logger->error("Spadla procedura nebo sklípek. Msg: {$e->getMessage()}");
                $error_docs[] = $name;
            }
        }

        if (!empty($error_docs)){
            throw new FileUploadException($error_docs, $stored_docs);
        }
    }


    public function saveDocument(Document $document):bool{
            $document_id = $this->sklipek->saveDokument(0, 0, 0, $document->getExtension(), $document->getContent())->dokumentId;
            $this->database->query(
                "procedura ?, ?, ?, ?, ?, ?"
                , $document->getName()
                , $document->getSize()
                , $document->getLink()
                , $document->getDomain()
                , $document_id
                , $document->getUserName()
            )->fetch();

        return empty($document_id);
    }


    public function deleteFile(int $id, string $username){
        try {
            $this->database->query(
                'procedura ?, ?'
                , $id
                , $username
            )->fetch();
        }catch (SybaseException $e){
            $this->logger->error("Spadla procedura: procedura {$id}, '{$username}'. DB msg: {$e->getMessage()}");
            throw new FileManException("Dokument se nepodařilo odstranit");
        }

    }

    public function getDocument(?int $id):Document{
        $doc = null;
        try {
            $res = $this->database->query(
                "procedura ?"
                , $id
            )->fetch();
            $content = $this->loadFromSklipek($res->S255_token, $id);

            $doc = new Document(
                $res->a,
                $res->b,
                $res->c,
                $res->d,
                $res->e,
                $content,
                $res->f,
                $res->g
            );
        }catch (SybaseException | FileManException $e) {
            $this->logger->error("Spadla procedura nebo sklípek. Msg: {$e->getMessage()}");
        }

        return $doc;
    }


    public function loadFromSklipek($token, $id_cms, $UID = null){
        try{
            $ret = $this->sklipek->loadDokument($token, $id_cms);
            return $ret->content;
        } catch(SklipekException $e){
            throw new FileManException(message:$e->getMessage() . " IDCMS:" . $id_cms);
        }
    }


}

class FileUploadException extends \Exception{
    private array $errorFiles = [];
    private array $successFiles = [];

    public function __construct(array $errorFiles = [], array $successFiles = [], string $message = "" ) {
        parent::__construct();
        $this->errorFiles = $errorFiles;
        $this->successFiles = $successFiles;
    }


    public function getErrorFiles(): string {
        return implode(",", $this->errorFiles);
    }


    public function getSuccessFiles(): string {
        return implode(",", $this->successFiles);
    }
}

class FileManException extends \Exception{
    public function __construct($message = "", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

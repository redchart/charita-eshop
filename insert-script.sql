SET FOREIGN_KEY_CHECKS=0; 

insert into UserRoles (Title, Description) values ('sysadmin', 'Admin of the whole system');
insert into UserRoles (Title, Description) values ('foundation_admin', 'Admin of the foundation');
insert into UserRoles (Title, Description) values ('customer', 'Customer');


insert into UserStates (Title) values ('unverified');
insert into UserStates (Title) values ('verified');
insert into UserStates (Title) values ('blocked');


insert into Users (Email, Password, Name, Surname, UserRoleID, UserStateID) values ('foundation@foundation.com', 'admin', 'Admin', 'Foundation', 2, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('rscallan1@reference.com', 'YWqh56j', 'Rozanna', 'Scallan', 2, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('eboutellier2@boston.com', 'Yov7q7', 'Ely', 'Boutellier', 2, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('lmaccome3@paginegialle.it', 'WN3tKOnNJ', 'Lesley', 'MacCome', 2, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('mshoebrook4@a8.net', 'ncQKmZO', 'Modesta', 'Shoebrook', 2, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('elissett5@timesonline.co.uk', 'nfneO2agHiuo', 'Elia', 'Lissett', 2, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('sohern6@spotify.com', '7JTaDbmpOe', 'Sauncho', 'O''Hern', 2, 3);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('gpyrah7@hhs.gov', 'lM4qMDOX3hVP', 'Gwyneth', 'Pyrah', 2, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('drosendorf8@ifeng.com', 'RsNu1GaCi', 'Drew', 'Rosendorf', 2, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('lmaccathay9@tuttocitta.it', 'lpnIdBvafh7q', 'Loni', 'MacCathay', 2, 1);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('zcovottia@blinklist.com', 'RglZcW2hMY', 'Zachery', 'Covotti', 3, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('mmccallisterb@discuz.net', '42i56TQp', 'Mable', 'McCallister', 3, 1);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('smartynkac@aboutads.info', 'FkfChW', 'Shani', 'Martynka', 3, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('rnicholesd@slideshare.net', '9sheWnx', 'Rod', 'Nicholes', 3, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('vcomiskeye@prnewswire.com', 'YMucIIwCGRfA', 'Valery', 'Comiskey', 3, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('dcharleyf@newyorker.com', 'rG41ICoD7J0Y', 'Deeanne', 'Charley', 3, 3);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('lreubeng@qq.com', 'YH16DH8', 'Lenard', 'Reuben', 3, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('pmattiussih@redcross.org', 't49Er5m', 'Patrice', 'Mattiussi', 3, 3);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('scatlini@unc.edu', 'hKXLE4', 'Shara', 'Catlin',3, 3);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('dhoodspethj@ihg.com', 'YKh573wA', 'Dom', 'Hoodspeth', 3, 1);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('ooakesk@clickbank.net', '51r1ElIuW', 'Orson', 'Oakes', 3, 3);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('kkearneyl@wired.com', 'VkafGD0FmN', 'Kristofor', 'Kearney', 3, 3);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('zgowdym@mysql.com', 'YMVg6ahUF', 'Zachary', 'Gowdy', 3, 3);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('ddeniskevichn@blogspot.com', 'atBQcT', 'Dean', 'Deniskevich', 3, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('fbeacocko@chron.com', 'ZSGn40C', 'Fan', 'Beacock', 3, 1);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('mlougheidp@google.com.au', 'VWEmfcyuyFQ', 'Miguel', 'Lougheid', 3, 1);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('jpilchq@springer.com', 'dxfTmSDM', 'Juieta', 'Pilch', 3, 3);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('tsandeverr@elegantthemes.com', '9YXg2QbfF9', 'Teresina', 'Sandever', 3, 2);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('smorsons@economist.com', 'Ctpg7bRiupA', 'Serena', 'Morson', 3, 3);
insert into Users (Email, Password, Name, Surname,  UserRoleID, UserStateID) values ('admin@sysadmin.com', 'admin', 'Admin', 'Sysadmin', 1, 2);


insert into Foundations (Image, Name, Address, Email, Phone, UserID, Description) values ('assets/foundations-imgs/1.png', 'Útulkáči', '87364 Eliot Plaza', 'moshiels0@kickstarter.com', '4516608154', 1, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Nam sed tellus id magna elementum tincidunt. Nullam at arcu a est sollicitudin euismod. In rutrum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Integer imperdiet lectus quis justo. Phasellus faucibus molestie nisl. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat.');
insert into Foundations (Image, Name, Address, Email, Phone, UserID, Description) values ('assets/foundations-imgs/2.png', 'Toulavé kotě', '3 Vermont Hill', 'jthurlbeck1@google.ru', '8233159938', 2, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Nam sed tellus id magna elementum tincidunt. Nullam at arcu a est sollicitudin euismod. In rutrum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Integer imperdiet lectus quis justo. Phasellus faucibus molestie nisl. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat.');
insert into Foundations (Image, Name, Address, Email, Phone, UserID, Description) values ('assets/foundations-imgs/3.png', 'Psí azyl', '89 Melody Alley', 'dkeywood2@ameblo.jp', '5425297655', 3, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Nam sed tellus id magna elementum tincidunt. Nullam at arcu a est sollicitudin euismod. In rutrum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Integer imperdiet lectus quis justo. Phasellus faucibus molestie nisl. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat.');
insert into Foundations (Image, Name, Address, Email, Phone, UserID, Description) values ('assets/foundations-imgs/4.png', 'Foundation4', '19533 Merchant Drive', 'rchastney3@shop-pro.jp', '7754999089', 4, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Nam sed tellus id magna elementum tincidunt. Nullam at arcu a est sollicitudin euismod. In rutrum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Integer imperdiet lectus quis justo. Phasellus faucibus molestie nisl. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat.');
insert into Foundations (Image, Name, Address, Email, Phone, UserID, Description) values ('assets/foundations-imgs/5.png', 'Foundation5', '95342 Manitowish Lane', 'cbairnsfather4@addthis.com', '7612071606', 5, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Nam sed tellus id magna elementum tincidunt. Nullam at arcu a est sollicitudin euismod. In rutrum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Integer imperdiet lectus quis justo. Phasellus faucibus molestie nisl. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat.');
insert into Foundations (Image, Name, Address, Email, Phone, UserID, Description) values ('assets/foundations-imgs/6.png', 'Foundation6','582 Coleman Street', 'afrancescuzzi5@list-manage.com', '6884522872', 6, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Nam sed tellus id magna elementum tincidunt. Nullam at arcu a est sollicitudin euismod. In rutrum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Integer imperdiet lectus quis justo. Phasellus faucibus molestie nisl. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat.');
insert into Foundations (Image, Name, Address, Email, Phone, UserID, Description) values ('assets/foundations-imgs/7.png', 'Foundation7','8580 Rusk Point', 'jbrussels6@google.co.uk', '4575938859', 7, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Nam sed tellus id magna elementum tincidunt. Nullam at arcu a est sollicitudin euismod. In rutrum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Integer imperdiet lectus quis justo. Phasellus faucibus molestie nisl. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat.');
insert into Foundations (Image, Name, Address, Email, Phone, UserID, Description) values ('assets/foundations-imgs/8.png', 'Foundation8','3786 Fallview Park', 'charcourt7@cocolog-nifty.com', '5784171281', 8, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Nam sed tellus id magna elementum tincidunt. Nullam at arcu a est sollicitudin euismod. In rutrum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Integer imperdiet lectus quis justo. Phasellus faucibus molestie nisl. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat.');
insert into Foundations (Image, Name, Address, Email, Phone, UserID, Description) values ('assets/foundations-imgs/9.png', 'Foundation9','1088 Oriole Avenue', 'fglyne8@seesaa.net', '1687680345', 9, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Nam sed tellus id magna elementum tincidunt. Nullam at arcu a est sollicitudin euismod. In rutrum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Integer imperdiet lectus quis justo. Phasellus faucibus molestie nisl. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat.');
insert into Foundations (Image, Name, Address, Email, Phone, UserID, Description) values ('assets/foundations-imgs/10.png', 'Foundation10','11649 Burning Wood Center', 'wdescroix9@dedecms.com', '1728306487', 10, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Integer rutrum, orci vestibulum ullamcorper ultricies, lacus quam ultricies odio, vitae placerat pede sem sit amet enim. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Duis ante orci, molestie vitae vehicula venenatis, tincidunt ac pede. Fusce dui leo, imperdiet in, aliquam sit amet, feugiat eu, orci. Nam sed tellus id magna elementum tincidunt. Nullam at arcu a est sollicitudin euismod. In rutrum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Integer imperdiet lectus quis justo. Phasellus faucibus molestie nisl. Fusce tellus odio, dapibus id fermentum quis, suscipit id erat.');


insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (16, 1, 6, 2, 29);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (7, 2, 1, 2, 16);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (81, 3, 1, 1, 4);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (59, 4, 5, 2, 6);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (98, 5, 7, 3, 29);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (68, 6, 2, 2, 27);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (37, 7, 4, 3, 19);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (54, 8, 1, 2, 23);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (10, 9, 8, 2, 17);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (77, 10, 8, 3, 14);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (16, 11, 9, 1, 13);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (93, 12, 10, 3, 22);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (62, 13, 5, 2, 17);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (1, 14, 8, 2, 5);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (85, 15, 7, 2, 2);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (5, 16, 4, 3, 21);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (16, 17, 4, 3, 22);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (79, 18, 5, 1, 20);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (91, 19, 9, 2, 28);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (87, 20, 5, 1, 25);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (61, 21, 7, 2, 30);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (65, 22, 2, 1, 13);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (18, 23, 2, 1, 11);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (64, 24, 2, 3, 24);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (90, 25, 4, 3, 26);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (16, 26, 9, 1, 16);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (83, 27, 7, 1, 20);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (25, 28, 1, 2, 30);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (27, 29, 8, 3, 27);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (48, 30, 7, 2, 4);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (72, 31, 10, 1, 20);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (38, 32, 9, 3, 9);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (24, 33, 5, 2, 8);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (73, 34, 9, 1, 29);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (19, 35, 2, 1, 21);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (41, 36, 3, 3, 28);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (25, 37, 8, 1, 12);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (80, 38, 3, 2, 16);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (5, 39, 1, 3, 27);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (11, 40, 10, 2, 19);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (100, 41, 5, 1, 15);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (57, 42, 9, 2, 29);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (41, 43, 1, 2, 14);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (11, 44, 8, 3, 25);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (63, 45, 3, 2, 3);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (46, 46, 4, 2, 16);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (74, 47, 2, 1, 11);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (29, 48, 2, 2, 14);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (33, 49, 5, 1, 15);
insert into Demands (Quantity, DemandID, FoundationID, DemandStateID, ProductID) values (45, 50, 1, 1, 29);


insert into OrderStates (Title, Description, OrderStateID) values ('Processing', 'Order is being processed at the stock!', 1);
insert into OrderStates (Title, Description, OrderStateID) values ('Shipping', 'Order is on its way to the foundation!', 2);
insert into OrderStates (Title, Description, OrderStateID) values ('Done', 'Order has been delivered!', 3);


insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/1.png', 242, 'Cobra, cape', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 1, 1);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/2.png', 207, 'Genet, small-spotted', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.

Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 2, 2);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/3.png', 788, 'Blue-tongued skink', 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 3, 3);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/4.png', 891, 'Green-backed heron', 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 4, 4);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/5.png', 715, 'Macaw, blue and gold', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 5, 5);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/6.png', 290, 'Common genet', 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 6, 6);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/7.png', 277, 'Red sheep', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.

Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', 7, 7);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/8.png', 823, 'Black-throated butcher bird', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 8, 8);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/9.png', 444, 'American bighorn sheep', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.

Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.

Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 9, 9);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 303, 'Crown of thorns starfish', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 10, 10);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 502, 'Wolf, timber', 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 11, 11);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 908, 'Gazelle, thomson''s', 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 12, 12);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 718, 'Roan antelope', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 13, 13);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 355, 'Galapagos dove', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.

Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 14, 14);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 690, 'Japanese macaque', 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.

Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', 15, 15);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 987, 'Mongoose, yellow', 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.

Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', 16, 16);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 549, 'Canada goose', 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat.

Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede.', 17, 17);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 985, 'Macaque, japanese', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 18, 18);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 478, 'Klipspringer', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 19, 19);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 412, 'Dove, galapagos', 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.

Sed ante. Vivamus tortor. Duis mattis egestas metus.

Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 20, 20);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 671, 'Dusky gull', 'Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 21, 21);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 816, 'Gull, herring', 'In congue. Etiam justo. Etiam pretium iaculis justo.

In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 22, 22);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 622, 'Bee-eater (unidentified)', 'Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 23, 23);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 523, 'Tern, white-winged', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 24, 24);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 819, 'Savannah deer', 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.

Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.

Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 25, 25);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 817, 'African ground squirrel (unidentified)', 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.

Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.

Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 26, 26);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 937, 'Secretary bird', 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.

Phasellus in felis. Donec semper sapien a libero. Nam dui.

Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 27, 27);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 625, 'Heron, green', 'Fusce consequat. Nulla nisl. Nunc nisl.

Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.

In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 28, 28);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 363, 'Ocelot', 'Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero.

Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.

In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 29, 29);
insert into Products (Image, Price, Name, Description, ProductID, StockID) values ('assets/products/10.png', 599, 'Rufous tree pie', 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.

Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 30, 30);

insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 315, 1);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:22:00', 32, 2);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:23:00', 32, 3);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:24:00', 492, 4);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:25:00', 328, 5);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-31 13:21:00', 294, 6);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-31 13:21:00', 378, 7);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-31 13:21:00', 409, 8);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-31 13:21:00', 297, 9);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-31 13:21:00', 430, 10);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 277, 11);
insert into Stock (LastChange, Quantity, StockID) values ('2021-01-30 13:21:00', 251, 12);
insert into Stock (LastChange, Quantity, StockID) values ('2021-01-30 13:21:00', 384, 13);
insert into Stock (LastChange, Quantity, StockID) values ('2021-01-30 13:21:00', 388, 14);
insert into Stock (LastChange, Quantity, StockID) values ('2021-01-30 13:21:00', 490, 15);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 428, 16);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 106, 17);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 2, 18);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 146, 19);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 256, 20);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 470, 21);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 42, 22);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 59, 23);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 78, 24);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 255, 25);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 321, 26);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 133, 27);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 19, 28);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 344, 29);
insert into Stock (LastChange, Quantity, StockID) values ('2021-10-30 13:21:00', 395, 30);


insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (767, '2021-10-30 13:21:00', 1, 15, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (1864, '2021-10-30 13:22:00', 2, 25, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (4266, '2021-10-30 13:21:00', 3, 15, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (4514, '2021-10-30 13:22:00', 4, 11, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (3215, '2021-10-30 13:21:00', 5, 18, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (2865, '2021-10-30 13:22:00', 6, 15, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (4374, '2021-01-30 13:21:00', 7, 14, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (2549, '2021-10-30 13:21:00', 8, 17, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (1770, '2021-01-30 13:21:00', 9, 25, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (1339, '2021-10-30 13:21:00', 10, 25, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (3181, '2021-10-30 13:22:00', 11, 20, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (670, '2021-10-30 13:21:00', 12, 19, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (4576, '2021-01-30 13:21:00', 13, 17, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (560, '2021-01-30 13:21:00', 14, 24, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (1871, '2021-01-30 13:21:00', 15, 26, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (3685, '2021-01-30 13:21:00', 16, 20, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (1272, '2021-10-30 13:21:00', 17, 14, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (1742, '2021-01-30 13:21:00', 18, 23, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (850, '2021-10-30 13:22:00', 19, 27, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (590, '2021-10-30 13:21:00', 20, 11, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (3479, '2021-10-30 13:22:00', 21, 24, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (3644, '2021-10-30 13:21:00', 22, 28, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (1169, '2021-01-30 13:21:00', 23, 22, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (846, '2021-10-30 13:21:00', 24, 11, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (796, '2021-10-30 13:22:00', 25, 28, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (4250, '2021-10-30 13:21:00', 26, 27, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (4590, '2021-01-30 13:21:00', 27, 23, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (2972, '2021-10-30 13:21:00', 28, 20, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (4014, '2021-10-30 13:21:00', 29, 13, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (2874, '2021-10-30 13:21:00', 30, 28, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (2877, '2021-10-30 13:21:00', 31, 18, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (2816, '2021-10-30 13:21:00', 32, 21, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (643, '2021-10-30 13:22:00', 33, 21, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (3564, '2021-01-30 13:21:00', 34, 18, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (2573, '2021-10-30 13:22:00', 35, 16, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (1272, '2021-10-30 13:21:00', 36, 19, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (4516, '2021-10-30 13:21:00', 37, 25, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (3910, '2021-10-30 13:21:00', 38, 19, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (2192, '2021-01-30 13:21:00', 39, 28, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (520, '2021-10-30 13:22:00', 40, 27, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (2898, '2021-01-30 13:21:00', 41, 12, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (3258, '2021-10-30 13:21:00', 42, 13, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (2944, '2021-10-30 13:21:00', 43, 28, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (3654, '2021-01-30 13:21:00', 44, 13, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (3270, '2021-10-30 13:21:00', 45, 24, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (2743, '2021-10-30 13:21:00', 46, 27, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (1402, '2021-10-30 13:21:00', 47, 14, 1);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (389, '2021-01-30 13:21:00', 48, 12, 2);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (3095, '2021-01-30 13:21:00', 49, 24, 3);
insert into Orders (Price, Date, OrderID, UserID, OrderStateID) values (4187, '2021-10-30 13:21:00', 50, 26, 1);


insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (40, 15, 1, 24);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (16, 1, 2, 6);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (64, 17, 3, 12);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (45, 17, 4, 1);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (78, 13, 5, 2);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (72, 3, 6, 21);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (35, 23, 7, 3);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (79, 8, 8, 7);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (71, 1, 9, 1);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (34, 17, 10, 11);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (6, 22, 11, 6);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (63, 8, 12, 12);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (61, 7, 13, 15);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (2, 24, 14, 2);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (95, 1, 15, 16);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (64, 22, 16, 16);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (45, 10, 17, 12);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (23, 4, 18, 18);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (39, 4, 19, 21);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (31, 12, 20, 13);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (9, 1, 21, 13);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (100, 6, 22, 17);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (63, 12, 23, 12);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (82, 9, 24, 16);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (67, 2, 25, 19);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (23, 10, 26, 5);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (96, 18, 27, 19);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (50, 22, 28, 11);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (5, 13, 29, 9);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (80, 3, 30, 18);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (61, 8, 31, 14);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (40, 12, 32, 18);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (51, 24, 33, 11);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (48, 12, 34, 10);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (83, 6, 35, 14);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (80, 19, 36, 19);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (28, 10, 37, 12);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (20, 22, 38, 25);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (14, 4, 39, 16);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (11, 17, 40, 5);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (21, 22, 41, 23);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (99, 24, 42, 2);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (74, 24, 43, 21);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (63, 6, 44, 5);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (13, 4, 45, 25);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (34, 4, 46, 10);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (60, 20, 47, 21);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (82, 12, 48, 7);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (97, 25, 49, 4);
insert into OrderDetails (Quantity, DemandID, OrderDetailID, OrderID) values (90, 14, 50, 20);



insert into DemandStates (Title, Description, DemandStateID) values ('Created', 'Order has been created and is waiting to be fulfilled', 1);
insert into DemandStates (Title, Description, DemandStateID) values ('Closed', 'Order has been fulfilled', 2);

SET FOREIGN_KEY_CHECKS=1; 
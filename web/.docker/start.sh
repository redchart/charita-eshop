#!/bin/bash

echo "*************************** install composer and setup ********************"
#composer self-update
composer install #--prefer-source

echo "*************************** install npm (wait few seconds) ********************"
npm install

echo "*************************** compile CSS, JS (wait few seconds) ********************"
npm run web

echo "*************************** copy fonts (bootstrap icons) ********************"

echo "*************************** DONE ********************"

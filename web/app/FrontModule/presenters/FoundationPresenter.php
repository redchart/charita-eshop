<?php

namespace App\FrontModule\Presenters;

use App\Models\DemandService;
use App\Models\FoundationService;
use App\Presenters\BaseFrontPresenter;
use Nette\DI\Attributes\Inject;

/**
 * Class FoundationPresenter
 * @package App\FrontModule\Presenters
 */
class FoundationPresenter extends BaseFrontPresenter{
    /**
     * @var FoundationService
     */
    #[Inject]
    public FoundationService $fs;

    /**
     * @var DemandService
     */
    #[Inject]
    public DemandService $demSer;


    public function actionDefault(){}

    /**
     * @param $id
     */
    public function renderDetail($id){
        $demands = $this->demSer->getAllDemands($id);
        $foundation = $this->fs->getFoundation($id);
        $this->template->add('found', $foundation);
        $this->template->add('demands', $demands);
    }
}
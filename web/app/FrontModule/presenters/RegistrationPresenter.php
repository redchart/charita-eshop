<?php

namespace App\FrontModule\Presenters;

use App\Presenters\BaseFrontPresenter;

/**
 * Class RegistrationPresenter
 * @package App\FrontModule\Presenters
 */
class RegistrationPresenter extends BaseFrontPresenter{
    public function actionDefault(){}
}
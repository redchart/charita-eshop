<?php

namespace App\FrontModule\Presenters;

use App\Models\FoundationService;
use App\Presenters\BaseFrontPresenter;
use Nette\DI\Attributes\Inject;

/**
 * Class HomepagePresenter
 * @package App\FrontModule\Presenters
 */
class HomepagePresenter extends BaseFrontPresenter{
    /**
     * @var FoundationService
     */
    #[Inject]
    public FoundationService $fs;

    public function actionDefault(){}
}
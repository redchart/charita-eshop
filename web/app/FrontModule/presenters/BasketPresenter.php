<?php

namespace App\FrontModule\Presenters;

use App\Entity\Order;
use App\Models\DemandService;
use App\Models\FoundationService;
use App\Models\OrderService;
use App\Models\ProductService;
use App\Models\UserService;
use App\Presenters\BaseFrontPresenter;
use Doctrine\DBAL\ConnectionException;
use Nette\Application\UI\Form;
use Nette\DI\Attributes\Inject;

/**
 * Class BasketPresenter
 * @package App\FrontModule\Presenters
 */
class BasketPresenter extends BaseFrontPresenter{

    /**
     * @var FoundationService
     */
    #[Inject]
    public FoundationService $fs;

    /**
     * @var DemandService
     */
    #[Inject]
    public DemandService $demSer;

    /**
     * @var ProductService
     */
    #[Inject]
    public ProductService $proSer;

    /**
     * @var UserService
     */
    #[Inject]
    public UserService $userSer;

    /**
     * @var OrderService
     */
    #[Inject]
    public OrderService $orSer;

    /**
     * @throws \Nette\Application\AbortException
     */
    public function startup() {
        parent::startup();

        $user = $this->getUser();
        if(!$user->isLoggedIn()){
            $this->flashMessage("Musíte se prihlásit", "danger");
            $this->redirect(":Front:Sign:in", ["backlink" => $this->storeRequest()]);
        }

        if(!$user->isAllowed('nakup')){
            $this->flashMessage("Nemáte přístup do košíku. Byl jste odhlášen", "danger");
            $user->logout(TRUE);
            $this->redirect(":Front:Sign:in", ["backlink" => $this->storeRequest()]);
        }
    }


    public function beforeRender() {
        parent::beforeRender();

    }

    public function actionDefault(){
        $httpRequest = $this->getHttpRequest();
        $orders = json_decode($httpRequest->getCookie("orders")) ?? [];
        $final_price = 0;
        $demand_id = 0;
        $products_amount = 0;


        foreach ($orders as $order){
            if(is_null($order)){
                continue;
            }

            $demand = $this->demSer->getDemandById($order->demand_id);
            $product = $this->proSer->getProduct($demand->getProductId());
            $foundation = $this->fs->getFoundation($demand->getFoundationId());
            $demand->setProduct($product);

            $price = $product->getPrice() * $order->quantity;
            $order->demand = $demand;
            $order->foundation = $foundation;
            $order->price = $price;
            $final_price += $price;
            $demand_id = $demand->getId();
            $products_amount = $order->quantity;
        }

        $user_name = $this->getUser()->getIdentity()->getData()['username'];
        $user_info = $this->userSer->getUser($user_name);
        $this->getUser()->getStorage();

        $this->template->add('orders', $orders);
        $this->template->add('user_info', $user_info);
        $this->template->add('final_price', $final_price);
        $this->template->add('demand_id', $demand_id);
        $this->template->add('products_amount', $products_amount);
        bdump($orders);
    }

    /**
     * @return Form
     */
    protected function createComponentCreateOrder(){
        $form = new Form();

        $form->addText("first_name")
            ->setRequired(TRUE);
        $form->addText("second_name")
            ->setRequired(TRUE);
        $form->addText("email")
            ->setRequired(TRUE);
        $form->addSubmit("submit");
        $form->addHidden('demand');
        $form->addHidden('final_price');
        $form->addHidden('products_amount');

        $form->onSuccess[] = function ($form){
            $this->createOrderSubmitted($form, $form->values);
        };

        return $form;
    }

    /**
     * @param Form $form
     * @param \stdClass $values
     * @throws \Doctrine\DBAL\Exception
     * @throws \Nette\Application\AbortException
     */
    public function createOrderSubmitted(Form $form, \stdClass $values){

        try{
            $order = new Order(1, $values->final_price, date("Y-m-d H:i:s", time()),$this->getUser()->getId(),1 );
            $order->setProdAmount($values->products_amount);
            $msg = $this->orSer->createOrder($order, $this->demSer->getDemandById($values->demand));
            $this->flashMessage($msg, 'success');
            $this->redirect(":Front:Basket:default");

        } catch(ConnectionException $e){
            $form->addError($e->getMessage());

            return;
        }
    }
}
<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;


/**
 * Class RouterFactory
 * @package App\Router
 */
final class RouterFactory
{
	use Nette\StaticClass;

    /**
     * @return RouteList
     */
    public static function createRouter(): RouteList
	{
		$router = new RouteList;

        $router->withModule('Admin')
            ->addRoute('/adm/<presenter>/<action>', 'Page:default')
            ->end();

		$router->withModule('Front')
            ->addRoute('<presenter>/<action>[/<id>]', [
                "presenter" => "Homepage",
                "action" => "default"
            ]);

		return $router;
	}
}

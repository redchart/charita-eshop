<?php


namespace App\Presenters;

use App\Models\FoundationService;
use App\Models\UserService;
use Contributte\Monolog\LoggerManager;
use Nette\Application\UI\Presenter;
use Nette\DI\Attributes\Inject;
use Psr\Log\LoggerInterface;


/**
 * Class BaseFrontPresenter
 * @package App\Presenters
 */
class BaseFrontPresenter extends Presenter {
    /**
     * @var UserService
     */
    #[Inject]
    public UserService $user;

    /**
     * @var LoggerManager
     */
    #[Inject]
    public LoggerManager $loggerMng;

    /**
     * @var FoundationService
     */
    #[Inject]
    public FoundationService $fs;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     *
     */
    public function startup() {
        parent::startup();
        $this->logger = $this->loggerMng->get("default");
        $foundations = $this->fs->getAllFoundations();
        $this->template->add('foundations', $foundations);
    }

}
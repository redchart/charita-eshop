<?php

namespace App\Presenters;

use App\Models\UserService;
use Contributte\Monolog\LoggerManager;
use Nette\Application\UI\Presenter;
use Nette\DI\Attributes\Inject;
use Psr\Log\LoggerInterface;


/**
 * Class BaseAdminPresenter
 * @package App\Presenters
 */
class BaseAdminPresenter extends Presenter{
    /**
     * @var UserService
     */
    #[Inject]
    public UserService $user;

    /**
     * @var LoggerManager
     */
    #[Inject]
    public LoggerManager $loggerMng;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @throws \Nette\Application\AbortException
     */
    public function startup() {
        parent::startup();
        $this->logger = $this->loggerMng->get("default");

        $user = $this->getUser();
        if(!$user->isLoggedIn()){
            $this->redirect(":Admin:Sign:in", ["backlink" => $this->storeRequest()]);
        }
        if(!$user->isAllowed('admin')){
            $this->flashMessage("Nemáte přístup do administrace. Byl jste odhlášen", "danger");
            $user->logout(TRUE);
            $this->redirect(":Admin:Sign:in", ["backlink" => $this->storeRequest()]);
        }

    }

}
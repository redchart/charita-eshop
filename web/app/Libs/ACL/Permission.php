<?php
namespace App;

use App\Models\UserService;
use Contributte\Monolog\LoggerManager;
use Psr\Log\LoggerInterface;
use Nette\Security\User;

/**
 * Class Permission
 * @package App
 */
class Permission {
    use \Nette\SmartObject;

    /**
     * @var mixed|string
     */
    public static $ADM = "";
    /**
     * @var mixed|string
     */
    public static $EDITOR = "";
    /**
     * @var mixed|string
     */
    public static $DEALING = "";

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var UserService
     */
    private UserService $admUserModel;
    /**
     * @var User
     */
    private User $user;

    /**
     * Permission constructor.
     * @param array $permisions
     * @param UserService $admUserModel
     * @param LoggerManager $logger
     * @param User $user
     */
    public function __construct(array $permisions, UserService $admUserModel, LoggerManager $logger, User $user){
        $this->admUserModel = $admUserModel;
        $this->user = $user;
        $this->logger = $logger->get("default");
        self::$ADM = $permisions["adm"];
        self::$DEALING = $permisions["dealing"];
        self::$EDITOR = $permisions["editor"];
    }

    /**
     * @param string $resource
     * @return bool
     */
    public function isAllowed(string $resource):bool {
        if(!$this->user->isLoggedIn()){
            return FALSE;
        }

        return $this->admUserModel->isAllowed($resource, $this->user->getId());
    }
}
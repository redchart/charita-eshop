<?php
namespace App;

use Contributte\Monolog\LoggerManager;
use Nette\Security\AuthenticationException;
use Nette\Security\SimpleIdentity;
use Nette\Utils\Strings;
use Psr\Log\LoggerInterface;
use App\Models\UserService;

/**
 * Class Authenticator
 * @package App
 */
final class Authenticator implements \Nette\Security\Authenticator{
    /**
     * @var UserService
     */
    private UserService $admLoginModel;
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * Authenticator constructor.
     * @param UserService $admLogin
     * @param LoggerManager $logger
     */
    public function __construct(UserService $admLogin, LoggerManager $logger){
        $this->admLoginModel = $admLogin;
        $this->logger = $logger->get("default");
    }


    /**
     * @param string $email
     * @param string $password
     * @return SimpleIdentity
     * @throws AuthenticationException
     */
    function authenticate(string $email, string $password): SimpleIdentity{
        $user = $this->admLoginModel->getUser(Strings::lower($email));

        // takovy uzivatel neexistuje
        if(!$user){
            throw new AuthenticationException("Email nebo heslo je chybně!", self::IDENTITY_NOT_FOUND);
        }

        if(!password_verify($password, $user->getPassword())){
            throw new AuthenticationException("heslo je chybně!", self::INVALID_CREDENTIAL);
        }

        $this->logger->info("Prihlaseni uzivatele " . $user->getEmail() . " (" . $user->getId() . ") bylo uspesne.");
        $identity = new SimpleIdentity($user->getId(), $user->role->getTitle(), ["username" => $user->getEmail(), "real_name" => $user->getFirstName() . $user->getSecondName()]);
        return $identity;
    }
}
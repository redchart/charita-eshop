<?php

namespace App\Entity;
use Nette\SmartObject;

/**
 * Class State
 * @package App\Entity
 */
class State{

    /**
     * State constructor.
     * @param int $id
     * @param string $title
     */
    public function __construct(
        private int $id,
        private string $title
    ) {
    }

    /**
     * @param \Nette\Database\Table\ActiveRow|null $activeRow
     * @return static|null
     */
    public static function create(?\Nette\Database\Table\ActiveRow $activeRow): ?self
    {
        if ($activeRow === null)
            return null;

        return new State(...$activeRow->toArray());
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

}

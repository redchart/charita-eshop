<?php
namespace App\Entity;

use Nette\SmartObject;

/**
 * Class Stock
 * @package App\Entity
 */
class Stock {

    /**
     * Stock constructor.
     * @param int $id
     * @param int $quantity
     * @param string $lastChange
     */
    public function __construct(
        private int $id,
        private int $quantity,
        private string $lastChange,
    ) {
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getQuantity(): int {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void {
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getLastChange(): string {
        return $this->lastChange;
    }

    /**
     * @param string $lastChange
     */
    public function setLastChange(string $lastChange): void {
        $this->lastChange = $lastChange;
    }

    /**
     * @param \Nette\Database\Table\ActiveRow|null $activeRow
     * @return static|null
     */
    public static function create(?\Nette\Database\Table\ActiveRow $activeRow): ?self {
        if ($activeRow === null)
            return null;

        return new Stock(...$activeRow->toArray());
    }
}
<?php

namespace App\Entity;

use Nette\SmartObject;

/**
 * Class Foundation
 * @package App\Entity
 */
class Foundation {

    /**
     * Foundation constructor.
     * @param int $id
     * @param string $address
     * @param string $email
     * @param string $name
     * @param string $image
     * @param string $description
     * @param string $phone
     * @param User $user
     */
    public function __construct(
        private int $id,
        private string $address,
        private string $email,
        private string $name,
        private string $image,
        private string $description,
        private string $phone,
        public User $user
    ) {
    }

    /**
     * @return User
     */
    public function getUser(): User {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAddress(): string {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getEmail(): string {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getImage(): string {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getDescription(): string {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getPhone(): string {
        return $this->phone;
    }

    /**
     * @param \Nette\Database\Table\ActiveRow|null $activeRow
     * @return static|null
     */
    public static function create(?\Nette\Database\Table\ActiveRow $activeRow): ?self {
        if ($activeRow === null)
            return null;

        return new Foundation(...$activeRow->toArray());
    }
}
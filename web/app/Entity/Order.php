<?php
namespace App\Entity;

use Nette\SmartObject;

/**
 * Class Order
 * @package App\Entity
 */
class Order {
    public const STATE_PROCESSING = 1;
    public const STATE_SHIPPING = 2;
    public const STATE_DONE= 3;

    /**
     * @var int
     */
    private int $prodAmount = 0;

    /**
     * Order constructor.
     * @param int $id
     * @param int $price
     * @param string $date
     * @param int $user_id
     * @param int $state_id
     */
    public function __construct(
        private int $id,
        private int $price,
        private string $date,
        private int $user_id,
        private int $state_id,
    ) {
    }

    /**
     * @return int
     */
    public function getProdAmount(): int {
        return $this->prodAmount;
    }

    /**
     * @param int $prodAmount
     */
    public function setProdAmount(int $prodAmount): void {
        $this->prodAmount = $prodAmount;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPrice(): int {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getDate(): string {
        return $this->date;
    }

    /**
     * @return int
     */
    public function getUserId(): int {
        return $this->user_id;
    }

    /**
     * @return int
     */
    public function getStateId(): int {
        return $this->state_id;
    }

    /**
     * @param \Nette\Database\Table\ActiveRow|null $activeRow
     * @return static|null
     */
    public static function create(?\Nette\Database\Table\ActiveRow $activeRow): ?self {
        if ($activeRow === null)
            return null;

        return new Order(...$activeRow->toArray());
    }
}

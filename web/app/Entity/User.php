<?php
namespace App\Entity;

use Nette\SmartObject;
/**
 * Class User
 * @package App\Entity
 */
class User{
    // public Role $role;
    /**
     * @var Foundation
     */
    public Foundation $foundation;

    /**
     * User constructor.
     * @param int $id
     * @param string $first_name
     * @param string $second_name
     * @param Role $role
     * @param string $password
     * @param string $email
     * @param int $state_id
     */
    public function __construct(
        private int $id,
        private string $first_name,
        private string $second_name,
        public Role $role,
        private string $password,
        private string $email,
        private int $state_id,
    ){}

    /**
     * @param Foundation $foundation
     */
    public function setFoundation(Foundation $foundation): void {
        $this->foundation = $foundation;
    }

    /**
     * @param \Nette\Database\Table\ActiveRow|null $activeRow
     * @return static|null
     */
    public static function create(?\Nette\Database\Table\ActiveRow $activeRow): ?self
    {
        if ($activeRow === null)
            return null;

        return new User(...$activeRow->toArray());
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string {
        return $this->first_name;
    }

    /**
     * @param Role $role
     */
    public function setRole(Role $role): void {
        $this->role = $role;
    }

    /**
     * @return int
     */
   /* public function getRoleId(): int {
        return $this->role_id;
    }*/

    /**
     * @return string
     */
    public function getSecondName(): string {
        return $this->second_name;
    }

    /**
     * @return string
     */
    public function getRole(): Role {
        return $this->role;
    }

    /**
     * @return string
     */
    public function getPassword(): string {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getEmail(): string {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getStateId(): int {
        return $this->state_id;
    }


}
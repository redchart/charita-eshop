<?php
namespace App\Entity;



use Nette\SmartObject;

/**
 * Class Demand
 * @package App\Entity
 */
class Demand {

    public const STATE_CREATED = 1;
    public const STATE_CLOSED = 2;

    /**
     * @var Product
     */
    private Product $product;

    /**
     * @return Product
     */
    public function getProduct(): Product {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void {
        $this->product = $product;
    }

    /**
     * Demand constructor.
     * @param int $id
     * @param int $quantity
     * @param int $ready
     * @param int $foundationId
     * @param int $stateId
     * @param int $productId
     */
    public function __construct(
        private int $id,
        private int $quantity,
        private int $ready,
        private int $foundationId,
        private int $stateId,
        private int $productId,
    ) {
    }

    /**
     * @return int
     */
    public function getReady(): int {
        return $this->ready;
    }

    /**
     * @param \Nette\Database\Table\ActiveRow|null $activeRow
     * @return static|null
     *
     */
    public static function create(?\Nette\Database\Table\ActiveRow $activeRow): ?self {
        if ($activeRow === null)
            return null;

        return new Demand(...$activeRow->toArray());
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getQuantity(): int {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getFoundationId(): int {
        return $this->foundationId;
    }

    /**
     * @return int
     */
    public function getStateId(): int {
        return $this->stateId;
    }

    /**
     * @return int
     */
    public function getProductId(): int {
        return $this->productId;
    }
}
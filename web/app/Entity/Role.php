<?php

namespace App\Entity;
use Nette\SmartObject;

/**
 * Class Role
 * @package App\Entity
 */
class Role{

    /**
     * Role constructor.
     * @param int $id
     * @param string $title
     * @param string $description
     */
    public function __construct(
        private int $id,
        private string $title,
        private string $description,
    ) {
    }

    /**
     * @param \Nette\Database\Table\ActiveRow|null $activeRow
     * @return static|null
     */
    public static function create(?\Nette\Database\Table\ActiveRow $activeRow): ?self
    {
        if ($activeRow === null)
            return null;

        return new Role(...$activeRow->toArray());
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string {
        return $this->description;
    }

}

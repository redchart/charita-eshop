<?php
namespace App\Entity;

use Nette\SmartObject;

/**
 * Class Product
 * @package App\Entity
 */
class Product {
    /**
     * Product constructor.
     * @param int $id
     * @param int $price
     * @param string $name
     * @param Stock $stock
     * @param string $image
     * @param string $description
     */
    public function __construct(
        private int $id,
        private int $price,
        private string $name,
        private Stock $stock,
        private string $image,
        private string $description,
    ) {
    }

    /**
     * @param \Nette\Database\Table\ActiveRow|null $activeRow
     * @return static|null
     */
    public static function create(?\Nette\Database\Table\ActiveRow $activeRow): ?self {
        if ($activeRow === null)
            return null;

        return new Product(...$activeRow->toArray());
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPrice(): int {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @return Stock
     */
    public function getStock(): Stock {
        return $this->stock;
    }

    /**
     * @return string
     */
    public function getImage(): string {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getDescription(): string {
        return $this->description;
    }
}
<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use App\Models\DemandService;
use App\Models\FoundationService;
use App\Models\ProductService;
use App\Models\UserService;
use App\Presenters\BaseAdminPresenter;
use Doctrine\DBAL\ConnectionException;
use Nette\Application\UI\Form;
use Nette\DI\Attributes\Inject;
use Nette\Security\AuthenticationException;

/**
 * Class PagePresenter
 * @package App\AdminModule\Presenters
 */
class PagePresenter extends BaseAdminPresenter{
    /**
     * @var UserService
     */
    #[Inject]
    public UserService $userSer;

    /**
     * @var FoundationService
     */
    #[Inject]
    public FoundationService $foundSer;

    /**
     * @var DemandService
     */
    #[Inject]
    public DemandService $demSer;

    /**
     * @var ProductService
     */
    #[Inject]
    public ProductService $prSer;

    /**
     *
     */
    public function beforeRender() {
        parent::beforeRender();
    }


    public function actionDefault(){
        $user = $this->getUser();
        $email = ($user->getIdentity()->getData()['username']);
        $admin = $this->userSer->getUser($email);

        $foundation = $this->foundSer->getFoundationByUser($admin->getId());
        $demands = $this->demSer->getAllDemands($foundation->getId());
        $products = $this->prSer->getAllProducts();

        $this->template->add("foundation", $foundation);
        $this->template->add("demands", $demands);
        $this->template->add("products", $products);
    }

    /**
     * @return Form
     */
    protected function createComponentCreateDemand(){
        $form = new Form();
        $productsObc = $this->prSer->getAllProducts();
        $products = [];
        foreach ($productsObc as $product){
            $products[$product->getId()] = $product->getName();
        }

        $form->addSelect("product", '', $products)
            ->setRequired(TRUE);
        $form->addPassword("quantity")
            ->setRequired(TRUE);

        $form->addHidden('foundation');

        $form->addSubmit("submit");

        $form->onSuccess[] = function ($form){
            $this->createDemandSubmitted($form, $form->values);
        };

        return $form;
    }

    /**
     * @param Form $form
     * @param \stdClass $values
     * @throws \Nette\Application\AbortException
     */
    public function createDemandSubmitted(Form $form, \stdClass $values){

        try{
            $this->demSer->createDemand((int) $values->foundation, (int) $values->product, (int) $values->quantity);
            $this->redirect(":Admin:Page:default");

        } catch(ConnectionException $e){
            $form->addError($e->getMessage());

            return;
        }
    }

    /**
     * @return Form
     */
    protected function createComponentUpdateDemand(){
        $form = new Form();
        $productsObc = $this->prSer->getAllProducts();
        $products = [];
        foreach ($productsObc as $product){
            $products[$product->getId()] = $product->getName();
        }

        $form->addSelect("product", '', $products)
            ->setRequired(TRUE)
            ->setDisabled(TRUE);
        $form->addPassword("quantity")
            ->setRequired(TRUE);

        $form->addHidden('foundation');
        $form->addHidden('demand');

        $form->addSubmit("update")->onClick[] = [$this, 'updateDemandSubmitted'];
        $form->addSubmit("delete")->onClick[] = [$this, 'deleteDemandSubmitted'];

        return $form;
    }

    /**
     * @param Form $form
     * @param \stdClass $values
     * @throws \Doctrine\DBAL\Exception
     * @throws \Nette\Application\AbortException
     */
    public function updateDemandSubmitted(Form $form, \stdClass $values){

        try{
            $msg = $this->demSer->updateDemand((int) $values->demand, (int) $values->quantity);
            $this->flashMessage($msg, 'success');
            $this->redirect(":Admin:Page:default");

        } catch(ConnectionException $e){
            $form->addError($e->getMessage());

            return;
        }
    }

    /**
     * @param Form $form
     * @param \stdClass $values
     * @throws \Doctrine\DBAL\Exception
     * @throws \Nette\Application\AbortException
     */
    public function deleteDemandSubmitted(Form $form, \stdClass $values){

        try{
            $msg = $this->demSer->deleteDemand((int) $values->demand);
            $this->flashMessage($msg, 'success');
            $this->redirect(":Admin:Page:default");

        } catch(ConnectionException $e){
            $form->addError($e->getMessage());

            return;
        }
    }
}
<?php
declare(strict_types=1);

namespace App\AdminModule\Presenters;

use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;
use Nette\Security\AuthenticationException;

/**
 * Class SignPresenter
 * @package App\AdminModule\Presenters
 */
class SignPresenter extends Presenter{
    /**
     *
     */
    public function beforeRender() {
        parent::beforeRender();
        $this->setLayout("signLayout");
    }

    /**
     * @throws \Nette\Application\AbortException
     */
    public function actionIn(){
        if($this->user->isLoggedIn()){
             //je prihlaseny a dostal se na login stranku -> presmerujem do administrace
             $this->redirect(":Admin:Page:default");
        }
    }

    /**
     * @throws \Nette\Application\AbortException
     */
    public function actionOut(){
        if($this->user->isLoggedIn()){
            $this->user->logout(true);
            $this->flashMessage("Byl jste odhlášen!", "success");
            $this->redirect(":Admin:Sign:in");
        }
    }

    /**
     * @return Form
     */
    protected function createComponentLoginForm(){
        $form = new Form();
        $form->addText("username", "username")
            ->setRequired(TRUE);
        $form->addPassword("password", "pass")
            ->setRequired(TRUE);

        $form->addSubmit("submit", "odeslat");

        $form->onSuccess[] = function ($form){
            $this->loginFormSubmitted($form, $form->values);
        };

        return $form;
    }

    /**
     * @param Form $form
     * @param \stdClass $values
     * @throws \Nette\Application\AbortException
     */
    public function loginFormSubmitted(Form $form, \stdClass $values){
        try{
            $this->user->login($values->username, $values->password);
            $this->user->setExpiration("+1 hour");
            $this->redirect(":Admin:Page:default");
        } catch(AuthenticationException $e){
            $form->addError($e->getMessage());
            return;
        }
    }
}
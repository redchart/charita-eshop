<?php

namespace App\Models;

use App\Entity\Demand;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Caching\Cache;
use Nette\Caching\Storage;
use Contributte\Monolog\LoggerManager;
use Psr\Log\LoggerInterface;

/**
 * Class DemandService
 * @package App\Models
 */
class DemandService{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var Cache
     */
    private Cache $cache;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * @var array
     */
    private array $allDemands = [];
    /**
     * @var ProductService
     */
    private ProductService $ps;

    /**
     * DemandService constructor.
     * @param LoggerManager $logger
     * @param Storage $storage
     * @param EntityManagerInterface $em
     * @param ProductService $ps
     */
    public function __construct(LoggerManager $logger, Storage $storage, EntityManagerInterface $em, ProductService $ps){
        $this->logger = $logger->get('default');
        $this->cache = new Cache($storage, 'Permissions');
        $this->em = $em;
        $this->ps = $ps;
    }

    /**
     * @param int $dem_id
     * @return Demand
     */
    public function getDemandById(int $dem_id):Demand {
        /** @var Demand|null $demand */
        $demand = $this->em->getRepository(Demand::class)->findOneBy(['id' => $dem_id]);
        if(is_null($demand)){
            return $demand;
        }

        return $demand;

    }

    /**
     * @param int $found_id
     * @return mixed
     */
    public function getAllDemands(int $found_id){
        /**
         * @var Demand[]  $res
         */
        $res =  $this->em->createQuery('SELECT d FROM App\Entity\Demand d WHERE d.foundationId = '.$found_id .'AND d.stateId = ' . Demand::STATE_CREATED)
            ->getResult();

        foreach ($res as $demand){
            /**
             * @var Product $product
             */
            $product = $this->ps->getProduct($demand->getProductId());
            $demand->setProduct($product);
            $this->allDemands[$found_id][$demand->getId()] = $demand;
        }
        return $this->allDemands[$found_id];
    }

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function createDemand(int $found_id, int $product_id, int $quantity){
        $conn = $this->em->getConnection();
        $queryBuilder = $conn->createQueryBuilder();

        $query = $queryBuilder->insert('Demands')
            ->values(
                [
                    'Quantity' => '?',
                    'Ready' => '?',
                    'FoundationID' => '?',
                    'DemandStateID' => '?',
                    'ProductID' => '?'
                ]
            )
            ->setParameter(0,$quantity)
            ->setParameter(1, 0)
            ->setParameter(2, $found_id)
            ->setParameter(3, Demand::STATE_CREATED)
            ->setParameter(4, $product_id);
        $query->execute();
    }

    /**
     * @param int $demand_id
     * @param int $quantity
     * @return string
     * @throws \Doctrine\DBAL\Exception
     */
    public function updateDemand(int $demand_id, int $quantity):string{
        $conn = $this->em->getConnection();
        $queryBuilder = $conn->createQueryBuilder();

        $query = $queryBuilder->update('Demands', 'd')
            ->set('d.Quantity', '?')
            ->where('d.DemandID = ' .  $demand_id)
            ->setParameter(0,$quantity);
        $query->execute();

        return "Poptávka byla úspěšně upravena";
    }

    /**
     * @param int $demand_id
     * @return string
     * @throws \Doctrine\DBAL\Exception
     */
    public function deleteDemand(int $demand_id):string{
        $conn = $this->em->getConnection();
        $queryBuilder = $conn->createQueryBuilder();

        $query = $queryBuilder->update('Demands', 'd')
            ->set('d.DemandStateID', '?')
            ->where('d.DemandID = ' .  $demand_id
            )
            ->setParameter(0,Demand::STATE_CLOSED);
        $query->execute();

        return "Poptávka byla úspěšně odstraněná";
    }

    /**
     * @param int $demand_id
     * @param int $ready
     * @throws \Doctrine\DBAL\Exception
     */
    public function updateDemandReady(int $demand_id, int $ready){
        $demand = $this->getDemandById($demand_id);
        $conn = $this->em->getConnection();
        $queryBuilder = $conn->createQueryBuilder();

        $query = $queryBuilder->update('Demands', 'd')
            ->set('d.Ready', '?')
            ->where('d.DemandID = ' .  $demand_id
            )
            ->setParameter(0,$ready + $demand->getReady());
        $query->execute();
    }

}
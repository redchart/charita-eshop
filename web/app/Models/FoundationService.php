<?php

namespace App\Models;

use App\Entity\Foundation;
use App\Entity\Role;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Caching\Cache;
use Nette\Caching\Storage;
use Nette\SmartObject;
use Contributte\Monolog\LoggerManager;
use Psr\Log\LoggerInterface;
use App\Entity\User;

/**
 * Class FoundationService
 * @package App\Models
 */
class FoundationService{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var Cache
     */
    private Cache $cache;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * FoundationService constructor.
     * @param LoggerManager $logger
     * @param Storage $storage
     * @param EntityManagerInterface $em
     */
    public function __construct(LoggerManager $logger, Storage $storage, EntityManagerInterface $em){
        $this->logger = $logger->get('default');
        $this->cache = new Cache($storage, 'Permissions');
        $this->em = $em;
    }

    /**
     * @return Foundation[]
     */
    public function getAllFoundations(){
        /**
         * @var Foundation[]  $res
         */
        $res =  $this->em->createQuery('SELECT d FROM App\Entity\Foundation d')
            ->getResult();

        return $res;
    }

    /**
     * @param int $id
     * @return Foundation|null
     */
    public function getFoundation(int $id): ?Foundation{

        /** @var Foundation|null $foundation */
        $foundation = $this->em->getRepository(Foundation::class)->findOneBy(['id' => $id]);
        if(is_null($foundation)){
            return $foundation;
        }

        return $foundation;
    }

    /**
     * @param int $id
     * @return Foundation|null
     */
    public function getFoundationByUser(int $id): ?Foundation{

        /** @var Foundation|null $foundation */
        $foundation = $this->em->getRepository(Foundation::class)->findOneBy(['id' => $id]);
        if(is_null($foundation)){
            return $foundation;
        }

        return $foundation;
    }

}
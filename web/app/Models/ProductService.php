<?php

namespace App\Models;

use App\Entity\Product;
use App\Entity\Stock;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Caching\Cache;
use Nette\Caching\Storage;
use Contributte\Monolog\LoggerManager;
use Psr\Log\LoggerInterface;

/**
 * Class ProductService
 * @package App\Models
 */
class ProductService{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var Cache
     */
    private Cache $cache;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * @var StockService
     */
    private StockService $ss;


    /**
     * ProductService constructor.
     * @param LoggerManager $logger
     * @param Storage $storage
     * @param EntityManagerInterface $em
     * @param StockService $ss
     */
    public function __construct(LoggerManager $logger, Storage $storage, EntityManagerInterface $em, StockService $ss){
        $this->logger = $logger->get('default');
        $this->cache = new Cache($storage, 'Permissions');
        $this->em = $em;
        $this->ss = $ss;
    }

    /**
     * @param int $id
     * @return Product|null
     */
    public function getProduct(int $id): ?Product{

        /** @var Product|null $product */
        $product = $this->em->getRepository(Product::class)->findOneBy(['id' => $id]);
        if(is_null($product)){
            return $product;
        }

        return $product;
    }

    /**
     * @return Product[]
     */
    public function getAllProducts(){
        /**
         * @var Product[]  $res
         */
        $res =  $this->em->createQuery('SELECT d FROM App\Entity\Product d')
            ->getResult();

        foreach ($res as $product){
            /**
             * @var Stock $stock
             */
            $stock = $this->ss->getStock($product->getStock()->getId());
            $product->getStock()->setQuantity($stock->getQuantity());
            $product->getStock()->setLastChange($stock->getLastChange());
        }
        return $res;
    }
}
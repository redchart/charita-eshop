<?php
namespace App\Models;

use Nette\Security\Permission;

/**
 * Class AuthorizatorFactory
 * @package App\Models
 */
class AuthorizatorFactory{
    /**
     * @var string
     */
    public static $ADM = "sysadmin";
    /**
     * @var string
     */
    public static $ADM_N = "foundation_admin";
    /**
     * @var string
     */
    public static $REG = "customer";
    /**
     * @var string
     */
    public static $GUEST = "guest";
    /**
     * @var string
     */
    public static $authenticated = "authenticated";

    /**
     * @return Permission
     */
    public static function create(): Permission{
        $acl = new Permission();

        $acl->addRole(self::$GUEST);
        $acl->addRole(self::$authenticated);
        $acl->addRole(self::$REG , self::$GUEST);
        $acl->addRole(self::$ADM_N , self::$REG);
        $acl->addRole(self::$ADM , self::$ADM_N);


        $acl->addResource('nadace');
        $acl->addResource('nakup');
        $acl->addResource('admin');

        $acl->allow(self::$ADM, ['admin']);
        $acl->allow(self::$ADM_N, ['admin' ,'nadace']);
        $acl->allow(self::$REG, ['nakup']);
        $acl->deny(self::$REG, ['nadace']);
        $acl->deny(self::$GUEST, ['admin','nakup', 'nadace']);

        return $acl;
    }
}
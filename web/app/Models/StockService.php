<?php

namespace App\Models;

use App\Entity\Stock;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Caching\Cache;
use Nette\Caching\Storage;
use Contributte\Monolog\LoggerManager;
use Psr\Log\LoggerInterface;

/**
 * Class StockService
 * @package App\Models
 */
class StockService{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var Cache
     */
    private Cache $cache;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * StockService constructor.
     * @param LoggerManager $logger
     * @param Storage $storage
     * @param EntityManagerInterface $em
     */
    public function __construct(LoggerManager $logger, Storage $storage, EntityManagerInterface $em){
        $this->logger = $logger->get('default');
        $this->cache = new Cache($storage, 'Permissions');
        $this->em = $em;
    }

    /**
     * @param int $id
     * @return Stock|null
     */
    public function getStock(int $id): ?Stock{

        /** @var Stock|null $state */
        $state = $this->em->getRepository(Stock::class)->findOneBy(['id' => $id]);
        if(is_null($state)){
            return $state;
        }
        return $state;
    }
}
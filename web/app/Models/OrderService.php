<?php

namespace App\Models;

use App\Entity\Demand;
use App\Entity\Order;
use App\Entity\Product;
use App\Entity\Stock;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Caching\Cache;
use Nette\Caching\Storage;
use Contributte\Monolog\LoggerManager;
use Psr\Log\LoggerInterface;

/**
 * Class OrderService
 * @package App\Models
 */
class OrderService{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var Cache
     */
    private Cache $cache;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;
    /**
     * @var DemandService
     */
    private DemandService $ds;


    /**
     * OrderService constructor.
     * @param LoggerManager $logger
     * @param Storage $storage
     * @param EntityManagerInterface $em
     * @param DemandService $ds
     */
    public function __construct(LoggerManager $logger, Storage $storage, EntityManagerInterface $em, DemandService $ds){
        $this->logger = $logger->get('default');
        $this->cache = new Cache($storage, 'Permissions');
        $this->em = $em;
        $this->ds = $ds;
    }

    /**
     * @param Order $order
     * @param Demand $demand
     * @return string
     * @throws \Doctrine\DBAL\Exception
     */
    public function createOrder(Order $order, Demand $demand):string{
        //update demand Ready property
        //bdump($order);
        $this->ds->updateDemandReady($demand->getId(),$order->getProdAmount());

        //create order
        $conn = $this->em->getConnection();
        $queryBuilder = $conn->createQueryBuilder();

        $query = $queryBuilder->insert('Orders')
            ->values(
                [
                    'Price' => '?',
                    'Date' => '?',
                    'UserID' => '?',
                    'OrderStateID' => '?',
                ]
            )
            ->setParameter(0,$order->getPrice())
            ->setParameter(1, $order->getDate())
            ->setParameter(2, $order->getUserId())
            ->setParameter(3, Order::STATE_PROCESSING);
        $query->execute();

        return "Objednávka byl úspěšně vytvořena";
    }
}
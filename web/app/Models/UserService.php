<?php

namespace App\Models;

use App\Entity\Role;
use Doctrine\ORM\EntityManagerInterface;
use Nette\Caching\Cache;
use Nette\Caching\Storage;
use Nette\SmartObject;
use Contributte\Monolog\LoggerManager;
use Psr\Log\LoggerInterface;
use App\Entity\User;

/**
 * Class UserService
 * @package App\Models
 */
class UserService{
    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;
    /**
     * @var Cache
     */
    private Cache $cache;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $em;

    /**
     * UserService constructor.
     * @param LoggerManager $logger
     * @param Storage $storage
     * @param EntityManagerInterface $em
     */
    public function __construct(LoggerManager $logger, Storage $storage, EntityManagerInterface $em){
        $this->logger = $logger->get('default');
        $this->cache = new Cache($storage, 'Permissions');
        $this->em = $em;
    }

    /**
     * @param string $email
     * @return User|null
     */
    public function getUser(string $email): ?User{

            /** @var User|null $user */
            $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
            if(is_null($user)){
                return $user;
            }
            /** @var Role|null $role */
            $role = $this->em->getRepository(Role::class)->findOneBy(['id' => $user->getRole()->getId()]);
            $user->setRole($role);
            return $user;
    }

}
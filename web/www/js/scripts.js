let products_amount = -1;

function updateProductsAmount(){
    let oof = document.getElementById("products_amount").value;
    if(products_amount == -1){
        document.getElementById("products_amount").value = oof;
    }else{
        document.getElementById("products_amount").value = products_amount;
    }

}
function delete_cookie( name, path, domain ) {
    if( get_cookie( name ) ) {
        document.cookie = name + "=" +
            ((path) ? ";path="+path:"")+
            ((domain)?";domain="+domain:"") +
            ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
    }
}

function createOrder(){
    setCookie('orders', null, 1);
}

function addToBasket($context){
    let cart =  $($context).closest('.card');
    let product_id = cart.find("input[name='product_id']")[0].value;
    let demand_id = cart.find("input[name='demand_id']")[0].value;
    let quantity = cart.find("input[name='quantity']")[0].value;
    let orders = {};
    if(getCookie('orders')) {
        orders = JSON.parse(getCookie('orders'));
    }else{
        orders = {};
    }
    let order = {'product_id':product_id, 'demand_id':demand_id, 'quantity':quantity};
    if(!orders){
        orders = {};
    }

   // let cart_demand_id = document.getElementById('demand_id');
   // cart_demand_id.value = demand_id;
    orders['"'+demand_id+ 'demand"'] = order;
    //orders.push(order);
    setCookie('orders', JSON.stringify(orders), 1);
    document.getElementById('items_in_basket').innerText = Object.keys(orders).length;
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function notImplemented() {
    alert("This page was not implemented.")
}


function submit_login() {
    alert("Uživatel se pokusil prihlasit.");
}

function updateBasket(context, order_id, demand_id, product_price){
    let current_quantity = parseInt(context.value);



    let quantity_val_id=`${demand_id}_quantity_val_${order_id}`;
    let price_val_id=`${demand_id}_price_val_${order_id}`;
    let start_quantity_val_id=`${demand_id}_start_quantity_val_${order_id}`;
    let quantity_input = document.getElementById(quantity_val_id);
    let start_quantity_input = document.getElementById(start_quantity_val_id);
    let start_quantity = parseInt(start_quantity_input.value);
    let diff = current_quantity - start_quantity;
    start_quantity_input.value = current_quantity;


    let price_input = document.getElementById(price_val_id);
    let final_price = document.getElementById('final_price');
    let final_basket_price = document.getElementById('final_basket_price');
    let final_basket_price_input = document.getElementById('final_basket_price_input');

    let final_price_val = final_price.innerText;
    let final_basket_price_val = final_basket_price.innerText;

    let orders = {};
    if(getCookie('orders')) {
        orders = JSON.parse(getCookie('orders'));
    }else{
        orders = {};
    }

    price_input.innerText = parseInt(current_quantity) * parseInt(product_price);
    orders['"'+demand_id+ 'demand"']['quantity'] = parseInt(current_quantity);


    final_price.innerText =   parseInt(final_price_val) + parseInt(diff) * parseInt(product_price);
    final_basket_price.innerText =   parseInt(final_price_val) + parseInt(diff) * parseInt(product_price);
    final_basket_price_input.value =  parseInt(final_price_val) + parseInt(diff) * parseInt(product_price);

    if(current_quantity === 0){
        document.getElementById('items_in_basket').innerText = document.getElementById('items_in_basket').innerText - 1;
         delete orders['"'+demand_id+ 'demand"'];
         let tr = $(context).closest('tr');
         tr.remove();

    }
    setCookie('orders', JSON.stringify(orders), 1);
    products_amount = current_quantity;
}

function removeFromBasket(context, order_id, demand_id){
    let quantity_val_id=`${demand_id}_quantity_val_${order_id}`;
    let price_val_id=`${demand_id}_price_val_${order_id}`;

    let quantity_input = document.getElementById(quantity_val_id);
    let price_input = document.getElementById(price_val_id);
    let final_price = document.getElementById('final_price');
    let final_price_val = final_price.innerText;

    let final_basket_price = document.getElementById('final_basket_price');
    let orders = {};
    if(getCookie('orders')) {
        orders = JSON.parse(getCookie('orders'));
    }else{
        orders = {};
    }

    //let new_orders = orders.splice(order_id, 1); // 2nd parameter means remove one item only
    delete orders['"'+demand_id+ 'demand"'];
    final_price_val = final_price_val - price_input.innerText;
    final_price.innerText = final_price_val;
    final_basket_price.innerText = final_price_val;
    final_basket_price_input.value = final_price_val;

    document.getElementById('items_in_basket').innerText = document.getElementById('items_in_basket').innerText - 1;
    let tr = $(context).closest('tr');
    tr.remove();

    setCookie('orders', JSON.stringify(orders), 1);

}

function setDemandId(demand, ready, quantity, product){
    document.getElementById('current_demand').value = demand;
    document.getElementById('quantity_input').setAttribute('max', quantity * 2);
    document.getElementById('quantity_input').value = quantity;
    document.getElementById('quantity_input').setAttribute('min', ready > 0 ? ready : 1 );
    let select = document.getElementById('product_select');
    select.setAttribute('disabled', false);
    select.value = product
    select.setAttribute('disabled', true);

    document.getElementById('deleteDemand').hidden = ready > 0;

}


//add new demand in Admin window
$(function () {
    $("#addDemand").click(function () {
        div = document.createElement('div');
        $(div).addClass("container-sm demand").html("new inner div");
        $("#demand").append(div);
    });
    let orders = {};
    if(getCookie('orders')) {
        orders = JSON.parse(getCookie('orders'));
    }else{
        orders = {};
    }
    let length = 0;
    if(orders){
        length = orders.length;
    }
    if(document.getElementById('items_in_basket') && orders) {
        document.getElementById('items_in_basket').innerText = Object.keys(orders).length ? Object.keys(orders).length : 0;
    }
});


//add/substract amount of product in order (buttons + -)
$('.btn-number').click(function (e) {
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type = $(this).attr('data-type');
    var input_group = $(this).closest('.input-group');
    var input = input_group.find("input[name='" + fieldName + "']");
    var currentVal = parseInt(input.val());

    if (!isNaN(currentVal)) {
        if (type == 'minus') {
            if (currentVal > input.attr('min'))
                input.val(currentVal - 1).change();  

        } else if (type == 'plus') {
            if (currentVal < input.attr('max')) 
                input.val(currentVal + 1).change();
        }
    } else {
        input.val(1);
    }
});

$('.input-number').focusin(function () {
    $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function () {

    minValue = parseInt($(this).attr('min'));
    maxValue = parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    name = $(this).attr('name');

});
$(".input-number").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
    }
});

// reset amount after added to basket
$('.btn-num-reset').click(function () {
    fieldName = $(this).attr('data-field');
    type = $(this).attr('data-type');
    var input = $("input[name='" + fieldName + "']");
    input.val(1).change();
});


